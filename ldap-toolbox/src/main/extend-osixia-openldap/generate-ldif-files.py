#!/usr/bin/python

from optparse import OptionParser

import jinja2
import os


def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './'),
        variable_start_string='[%',
        variable_end_string='%]',
        line_comment_prefix='#'
    ).get_template(filename).render(context)


parser = OptionParser()
parser.add_option("--usernames", dest="usernames", help="User names file")
parser.add_option("--template", dest="template", help="Template file")
parser.add_option("--target-dir", dest="target_dir", help="Target directory")
(options, args) = parser.parse_args()

with open(options.usernames) as fp:
    for line in fp:
        line = line.strip()
        if len(line) == 0:
            continue
        if line.startswith('#'):
            continue
        s = line.split('=')
        ctx = {
            'uid': line,
            'cn': line.capitalize(),
            'sn': line.capitalize(),
            'email': line.lower()
        }

        outfile = open(options.target_dir + '/' + line + '.ldif', 'w')
        outfile.write(render(options.template, ctx).encode('utf-8'))
        outfile.close()

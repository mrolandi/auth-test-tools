# OpenLDAP Docker container

This project builds OpenLDAP Docker image with pre-defined user data. See https://github.com/osixia/docker-openldap

Generate ldif files from a list of usernames and a template:

```
generate-ldif-files.py --usernames usernames.txt --template templates/username.ldif.template --target-dir bootstrap/ldif/
```

Build the image `phenixid/openldap:latest` with `make build`, then start a container with the following command:

```
docker run --name openldap --env LDAP_TLS=false --detach -p 389:389 phenixid/openldap:latest
```


## Pro tip

The image will start the OpenLDAP server from the LDIF files, so if there are many such files, the startup might take a 
long time. Here's a tip to remedy this: 

1. Start the server and tail the log with `docker logs -f <container id>`. 
When the line `*** Running /container/run/process/slapd/run...`)` is printed, the server is fully up and running.

2. Attach to the running container with `docker exec -it <container id> /bin/bash` and delete all the ldif files with
the command `rm -r container/service/slapd/assets/config/bootstrap/ldif/*`.

3. Detach from the running container and commit the changes to a new image: 
```
docker commit <container id> phenixid/openldap:<new tag>
```


Now, starting a container from the `<new tag>` image should be a matter of a few seconds. 



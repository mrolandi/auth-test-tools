#!/usr/bin/python

import logging.handlers
import random
import re
import string

import os


def wrap(iterable, chunk_size):
    """ Each call yields the next chunk of the iterable. Wraps around and iterates forever. """
    n = 0
    while True:
        m = n + chunk_size
        result = iterable[n:m]
        result.extend(iterable[0:max(0, chunk_size - len(result))])
        yield result
        n = m % len(iterable)


def map_attributes(item, attributes):
    ldif_data = {}
    for key in attributes:
        attribute = attributes[key]
        if attribute in item:
            ldif_data[key] = item[attribute]
        elif callable(attribute):
            ldif_data[key] = attribute(item)
        else:
            ldif_data[key] = attribute
    return ldif_data


def user_entry(root, rdn, rdn_attribute):
    return {
        'rdn': rdn,
        'dn': rdn_attribute + '=' + rdn + ',' + root
    }


def group_entry(root, rdn, rdn_attribute):
    return {
        'rdn': rdn,
        'dn': rdn_attribute + '=' + rdn + ',' + root,
        'member': set()
    }


# noinspection PyUnusedLocal
def random_user_entry(root, rdn_attribute):
    rdn = ''.join(random.choice(string.lowercase) for x in range(10))
    return user_entry(root, rdn, rdn_attribute)


# noinspection PyUnusedLocal
def random_group_entry(root, rdn_attribute):
    rdn = ''.join(random.choice(string.lowercase) for x in range(10))
    return group_entry(root, rdn, rdn_attribute)


# noinspection PyUnusedLocal
def random_groups(number, root_dn, rdn_attribute='cn'):
    return [random_group_entry(root_dn, rdn_attribute) for x in range(number)]


# noinspection PyUnusedLocal
def random_users(number, root_dn, rdn_attribute='cn'):
    return [random_user_entry(root_dn, rdn_attribute) for x in range(number)]


def add_group_members(members, groups):
    """ Add all members to all groups """
    [x['member'].update([item['dn'] for item in members]) for x in groups]


def distribute_group_members(members, groups, chunk_size):
    """ Add members to all groups in chunks. Wrap around if the end of members list is reached. """
    member_iterator = wrap(members, chunk_size)
    for group in groups:
        next_chunk = member_iterator.next()
        group['member'].update([item['dn'] for item in next_chunk])


def init_printer(log_file_name, to_console=False, max_bytes=314572800, backup_count=9999):
    """ Create logger with RotatingFileHandler """
    log_level = logging.INFO
    format_string = '%(message)s'
    log_formatter = logging.Formatter(format_string)
    logger = logging.getLogger('ldif_printer')
    if to_console:
        logging.basicConfig(level=log_level, format=format_string)
    else:
        file_handler = logging.handlers.RotatingFileHandler(
            log_file_name,
            maxBytes=max_bytes,
            backupCount=backup_count,
            delay=True
        )
        file_handler.setFormatter(log_formatter)
        logger.addHandler(file_handler)
    logger.setLevel(log_level)
    return logger


def print_user(user, ldif_printer, object_class='organizationalPerson', extra_attributes=None):
    if extra_attributes is None:
        extra_attributes = {}
    ldif_data = map_attributes(user, extra_attributes)
    out_str = 'dn: %s\n'
    args = [user['dn']]
    out_str += 'objectClass: %s\n'
    args.append(object_class)
    for key in ldif_data:
        out_str += '%s: %s\n'
        args.append(key)
        args.append(ldif_data[key])
    ldif_printer.info(out_str + '\n', *args)


def print_group(group, ldif_printer, object_class='group', extra_attributes=None):
    ldif_data = map_attributes(group, extra_attributes)
    out_str = 'dn: %s\n'
    args = [group['dn']]
    out_str += 'objectClass: %s\n'
    args.append(object_class)
    for key in ldif_data:
        out_str += '%s: %s\n'
        args.append(key)
        args.append(ldif_data[key])
    for member in list(group['member']):
        out_str += 'member: %s\n'
        args.append(member)
    ldif_printer.info(out_str + '\n', *args)


def shutdown_printer(ldif_printer):
    """ Do a final rollover, then zero pad all file numbers (file extensions) to allow ordered import of LDIF data """
    ldif_printer.handlers[0].doRollover()
    base_file_name = ldif_printer.handlers[0].baseFilename
    print_file = os.path.basename(base_file_name)
    print_dir = os.path.dirname(base_file_name)
    pattern = '^' + print_file + '[\.][0-9]*'
    compiled_pattern = re.compile(pattern)
    file_names = os.listdir(print_dir)
    ldif_files = filter(lambda file_name: compiled_pattern.match(file_name), file_names)
    for ldif_file in ldif_files:
        s = ldif_file.split('.')
        old_file = os.path.join(print_dir, ldif_file)
        new_file = base_file_name + '.' + s[len(s) - 1].zfill(4)
        os.rename(old_file, new_file)
    logging.shutdown()

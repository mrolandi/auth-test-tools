#!/usr/bin/python

"""Generate AD user and group data

Run this script to generate LDIF files containing user and group data. The program will print to a single file
until the file size is approx. 300M, and then roll over to a new file. Each file will have a integer file extension
(e.g. "my_test_organization.ldif.23") that will be converted to a zero padded number when the program finishes.
The zero padding allows the files to be imported in a given order. If the program is edited, make sure that
the data is written in a correct order: no AD object can be referenced before it has been created.

"""

from itertools import chain
from optparse import OptionParser

from ldifgen import *

DESCRIPTION = 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...'


# import cmd:
# > ldifde -i -f file.ldif -k

# noinspection PyUnusedLocal
def random_user_data(number):
    return [{
        # 'City': ''.join(random.choice(string.lowercase) for x in range(10)).title(),
        'countryCode': 4,
        'title': ''.join(random.choice(string.lowercase) for x in range(5)).title(),
        'givenName': ''.join(random.choice(string.lowercase) for x in range(10)).title(),
        'displayName': ''.join(random.choice(string.lowercase) for x in range(6)).title() +
                       ' ' + ''.join(random.choice(string.lowercase) for x in range(10)).title(),
        'userPassword': ''.join(random.choice(string.lowercase) for x in range(23)).title(),
        'streetAddress': ''.join(random.choice(string.lowercase) for x in range(10)).title() + ' 123',
        'postalAddress': ''.join(random.choice(string.lowercase) for x in range(13)).title(),
        'telephoneNumber': ''.join(str(random.randint(0, 9)) for x in range(10)).title(),
        'mobile': ''.join(str(random.randint(0, 9)) for x in range(12)).title()
    } for x in range(number)]


parser = OptionParser()
parser.add_option('--ou', dest='organizational_unit')
parser.add_option('--out-directory', dest='out_directory')
(options, args) = parser.parse_args()

user_attributes = {'cn': 'rdn', 'description': DESCRIPTION, 'sn': lambda user: user['rdn'].title()}
group_attributes = {'cn': 'rdn', 'description': DESCRIPTION}

ldif_printer = init_printer(options.out_directory + options.organizational_unit + '.ldif', to_console=False)

group = options.organizational_unit
parent_groups = random_groups(number=100,
                              root_dn='CN=ParentGroups,CN=TestGroups,OU=' + group + ',DC=company,DC=local')
sub_groups = random_groups(number=1000, root_dn='CN=ChildGroups,CN=TestGroups,OU=' + group + ',DC=company,DC=local')
all_users = random_users(number=200, root_dn='CN=' + group + ',CN=Users,DC=company,DC=local')

distribute_group_members(members=all_users, groups=sub_groups, chunk_size=20)
distribute_group_members(members=sub_groups, groups=parent_groups, chunk_size=10)

user_data = random_user_data(100)

for item in all_users:
    user_attributes.update(user_data[random.randint(0, len(user_data) - 1)])
    print_user(item, ldif_printer, extra_attributes=user_attributes)

for item in chain(sub_groups, parent_groups):
    print_group(item, ldif_printer, extra_attributes=group_attributes)

shutdown_printer(ldif_printer)

# Create AD containers in a structure suitable for generated test data,
# intended for easy overview of generated users and groups.

param (
    [string]$domain = "DC=company,DC=local",
    [string]$groupName
)
New-ADObject -Path "$domain" -Type organizationalUnit -Name $groupName -DisplayName $groupName
New-ADObject -Path "CN=Users,$domain" -Type container -Name $groupName -DisplayName $groupName
New-ADObject -Path "OU=$groupName,$domain" -Type container -Name TestGroups -DisplayName TestGroups
New-ADObject -Path "CN=TestGroups,OU=$groupName,$domain" -Type container -Name ChildGroups -DisplayName ChildGroups
New-ADObject -Path "CN=TestGroups,OU=$groupName,$domain" -Type container -Name ParentGroups -DisplayName ParentGroups

# Import all files in a single directory, using LDIFDE.
# It is assumed that the file names imply the import order.

param (
    [string]$directory
)
Get-ChildItem $directory | Sort-Object -Descending | foreach {  ldifde -i -f  $directory$_ -k}
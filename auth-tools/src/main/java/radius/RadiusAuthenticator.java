package radius;

import util.Dispatcher;
import util.Sampler;

import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RadiusAuthenticator {

    private static final Logger MONITOR = Logger.getLogger("monitor");

    private final Dispatcher<User> radiusDispatcher;
    private final RadAuth radAuth;

    public RadiusAuthenticator(String id, String hostName, int port, int socketTimeout, int retryCount, String secret, Sampler sampler) throws SocketException {
        sampler.addMonitored(id);
        radAuth = new RadAuth(hostName, port, socketTimeout, retryCount, secret);
        radiusDispatcher = new Dispatcher<>(s -> {
            long duration = 0;
            try {
                MONITOR.log(Level.FINE, String.format("Authenticating %s", s.name));
                long start = System.currentTimeMillis();
                radAuth.authenticate(s.name, s.pass);
                duration = System.currentTimeMillis() - start;
                sampler.addSuccess(duration);
            } catch (Exception e) {
                sampler.addError();
            }
        }, () -> sampler.removeMonitored(id));
    }

    public void run() {
        try {
            radiusDispatcher.processQueue();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void send(String username, String password, long queuedAt) {
        MONITOR.log(Level.FINER, username + "\tRADIUS\t" + radAuth.getTarget());
        radiusDispatcher.dispatch(new User(username, password), queuedAt);
    }

    public void stop() {
        radiusDispatcher.stop(true);
    }

    public static class User {
        public final String name;
        public final String pass;

        public User(String name, String pass) {
            this.name = name;
            this.pass = pass;
        }
    }
}

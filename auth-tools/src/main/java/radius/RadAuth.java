package radius;

import org.tinyradius.packet.AccessRequest;
import org.tinyradius.packet.RadiusPacket;
import org.tinyradius.util.RadiusClient;

import java.net.SocketException;
import java.util.concurrent.TimeUnit;

import static util.TOTP.*;

public class RadAuth {
    private final RadiusClient client;

    public RadAuth(String hostName, int port, int socketTimeout, int retryCount, String radiusSecret) throws SocketException {
        client = new DiscardingRadiusClient(hostName, radiusSecret);
        client.setAuthPort(port);
        client.setSocketTimeout(socketTimeout);
        client.setRetryCount(retryCount);
    }

    public String getTarget() {
        return client.getHostName() + ":" + client.getAuthPort();
    }

    public String authenticate(String user, String secretToken, int length) {
        try {
            long currentTimeInSecond = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
            long time = currentTimeInSecond / 3L;
            byte[] bytes = toBytes(secretToken);
            int otp = generateTOTP(bytes, time, length);
            AccessRequest request = new AccessRequest(user, padWithZeros(otp, length));
            RadiusPacket radiusResponse = client.authenticate(request);
            String reply = radiusResponse.getAttributeValue("Reply-Message");
            if (reply.contains("Access-Accept")) {
                return "Access-Accept";
            }
            if (reply.contains("Access-Reject")) {
                return "Access-Reject";
            }
            return reply;
        } catch (Exception ignored) {
            return "Error: " + ignored.getMessage();
        }
    }

    public String authenticate(String user, String password) {
        try {
            boolean radiusResponse = client.authenticate(user, password);
            if (radiusResponse) {
                return "Access-Accept";
            }
            return "Access-Reject";
        } catch (Exception ignored) {
            return "Error";
        }
    }
}

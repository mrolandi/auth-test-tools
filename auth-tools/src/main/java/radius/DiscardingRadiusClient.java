package radius;

import org.tinyradius.packet.AccessRequest;
import org.tinyradius.util.RadiusClient;
import org.tinyradius.util.RadiusException;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class DiscardingRadiusClient extends RadiusClient {

    public DiscardingRadiusClient(String hostName, String sharedSecret) {
        super(hostName, sharedSecret);
    }

    public synchronized boolean authenticate(String userName, String password) throws IOException {
        AccessRequest request = new AccessRequest(userName, password);
        DatagramPacket packetOut = this.makeDatagramPacket(request, getAuthPort());
        DatagramSocket socket = this.getSocket();
        int i = 1;

        while (i <= this.getRetryCount()) {
            try {
                socket.send(packetOut);
                return true;
            } catch (IOException var8) {
                if (i == this.getRetryCount()) {
                    throw var8;
                }
                ++i;
            }
        }
        return false;
    }
}

package util;

import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

@SuppressWarnings("unused")
public class LoggingConfig {
    public LoggingConfig() {
        try {
            String monitoringLevel = System.getProperty("monitoring.level", "INFO");
            System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tFT%1$tT.%1$tL\t%5$s %n");
            System.setProperty("java.util.logging.FileHandler.limit", "0");

            ConsoleHandler consoleHandler = new ConsoleHandler();
            consoleHandler.setLevel(Level.parse(monitoringLevel));
            consoleHandler.setFormatter(new SimpleFormatter());

            Logger sampler = Logger.getLogger("sampler");
            sampler.setLevel(Level.FINEST);
            sampler.addHandler(consoleHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

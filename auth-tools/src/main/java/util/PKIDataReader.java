package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Base64;

public class PKIDataReader {

    public final String fileName;
    private FileReader fileReader;
    private BufferedReader bufferedReader;
    private boolean finished = false;
    private final int maxRows;
    private int readRows = 0;

    public PKIDataReader(String fileName, int maxRows) {
        this.fileName = fileName;
        this.maxRows = maxRows;
    }

    public boolean next() throws IOException {
        if (fileReader == null) {
            reset();
        }
        String line;
        if (finished || (maxRows > -1 && ++readRows > maxRows) || (line = bufferedReader.readLine()) == null) {
            if (!finished) {
                finished = true;
            }
            close();
            return false;
        }
        char delimiter = '|';
        int i1 = ordinalIndexOf(line, delimiter, 5);
        int i2 = ordinalIndexOf(line, delimiter, 6);
        String payload = line.substring(i1 + 1, i2);
        byte[] pkiProfile = Base64.getDecoder().decode(payload);

        i1 = ordinalIndexOf(line, delimiter, 6);
        i2 = ordinalIndexOf(line, delimiter, 7);
        payload = line.substring(i1 + 1, i2);
        byte[] pkiAuth = Base64.getDecoder().decode(payload);

        i1 = ordinalIndexOf(line, delimiter, 7);
        payload = line.substring(i1 + 1);
        byte[] pkiSign = Base64.getDecoder().decode(payload);

        return true;
    }

    public void reset() throws IOException {
        if (fileReader != null) {
            fileReader.close();
            bufferedReader.close();
        }
        fileReader = new FileReader(new File(fileName));
        bufferedReader = new BufferedReader(fileReader);
        finished = false;
        readRows = 0;
    }

    public void close() throws IOException {
        fileReader.close();
        bufferedReader.close();
        fileReader = null;
        bufferedReader = null;
    }

    public static int ordinalIndexOf(String str, char c, int n) {
        int pos = str.indexOf(c);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(c, pos + 1);
        return pos;
    }
}

package util;

import java.util.LinkedList;
import java.util.function.Consumer;

public class Dispatcher<E> {

    private volatile boolean running = true;
    private volatile boolean killed = false;
    private final LinkedList<Envelope> queue = new LinkedList<>();
    private final Consumer<E> consumer;
    private final Runnable endHook;

    public Dispatcher(Consumer<E> consumer, Runnable endHook) {
        this.consumer = consumer;
        this.endHook = endHook;
    }

    public void stop(boolean gracefully) {
        running = false;
        killed = !gracefully;
        synchronized (queue) {
            queue.notify();
        }
    }

    public void dispatch(E item, long queuedAt) {
        queue.addLast(new Envelope(queuedAt, item));
        synchronized (queue) {
            queue.notify();
        }
    }

    public void processQueue() throws InterruptedException {
        while (running) {
            synchronized (queue) {
                if (running) {
                    queue.wait();
                }
            }
            while (!queue.isEmpty() && !killed) {
                Envelope e = queue.removeFirst();
                consumer.accept(e.item);
            }
        }
        if (endHook != null) {
            endHook.run();
        }
    }

    private class Envelope {
        private final long queuedAt;
        private final E item;

        private Envelope(long queuedAt, E item) {
            this.queuedAt = queuedAt;
            this.item = item;
        }
    }
}

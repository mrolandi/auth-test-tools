package util;


import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class TOTP {

    private static final int[] DIGITS_POWER
            // 0 1 2 3 4 5 6 7 8
            = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};

    public static void main(String[] args) {
        String secretToken = args[0];
        int length = Integer.parseInt(args[1]);
        long currentTimeInSecond = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        long time = currentTimeInSecond / 3L;
        byte[] bytes = toBytes(secretToken);
        int otp = generateTOTP(bytes, time, length);
        System.out.println("TOTP: " + otp);
    }

    private TOTP() {
    }

    /**
     * This method uses the JCE to provide the crypto algorithm. HMAC computes a
     * Hashed Message Authentication Code with the crypto hash algorithm as a
     * parameter.
     *
     * @param keyBytes : the bytes to use for the HMAC key
     * @param text     : the message or text to be authenticated
     */

    private static byte[] hmacSha(byte[] keyBytes, byte[] text) {
        try {
            Mac hmac = Mac.getInstance("HmacSHA1");
            SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
            hmac.init(macKey);
            return hmac.doFinal(text);
        } catch (GeneralSecurityException gse) {
            throw new UndeclaredThrowableException(gse);
        }
    }

    /**
     * This method generates a TOTP value for the given set of parameters.
     *
     * @param key    : the shared secret
     * @param time   : a value that reflects a time
     * @param digits : number of digits to return
     * @return: digits
     */

    public static int generateTOTP(byte[] key, long time, int digits) {

        byte[] msg = ByteBuffer.allocate(8).putLong(time).array();
        byte[] hash = hmacSha(key, msg);

        // put selected bytes into result int
        int offset = hash[hash.length - 1] & 0xf;

        int binary = ((hash[offset] & 0x7f) << 24) | ((hash[offset + 1] & 0xff) << 16) | ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

        return binary % DIGITS_POWER[digits];
    }

    public static byte[] toBytes(String hex) {
        byte[] bytes = new BigInteger("10" + hex, 16).toByteArray();
        int length = bytes.length - 1;
        byte[] copy = new byte[length];
        System.arraycopy(bytes, 1, copy, 0, length);
        return copy;
    }

    public static String padWithZeros(int i, int length) {
        if (i == 0) {
            char[] zeros = new char[length];
            Arrays.fill(zeros, '0');
            return new String(zeros);
        }
        for (int p = 1; p < length; p++) {
            if (i < Math.pow(10, p)) {
                char[] zeros = new char[length - p];
                Arrays.fill(zeros, '0');
                return new String(zeros) + Integer.toString(i);
            }
        }
        return Integer.toString(i);
    }

}
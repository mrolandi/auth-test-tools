package util;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Sampler {
    private static final Logger LOGGER = Logger.getLogger("sampler");

    private static final String HEADER = "mean_duration\taccess_accept\taccess_reject\terror";

    private final int samplingInterval;

    private boolean isRunning = true;

    private final AtomicInteger successCounter = new AtomicInteger(0);

    private final AtomicInteger failureCounter = new AtomicInteger(0);

    private final AtomicInteger errorCounter = new AtomicInteger(0);

    private final AtomicLong accumulator = new AtomicLong(0);

    private final AtomicInteger lastCheckPoint = new AtomicInteger(0);

    private final Set<String> monitored = ConcurrentHashMap.newKeySet();


    public Sampler(int samplingInterval) {
        this.samplingInterval = samplingInterval;
        LOGGER.log(Level.INFO, HEADER);
    }

    public void addMonitored(String monitored) {
        this.monitored.add(monitored);
    }

    public void removeMonitored(String monitored) {
        this.monitored.remove(monitored);
    }

    public void addSuccess(long value) {
        successCounter.incrementAndGet();
        accumulator.addAndGet(value);
    }

    public void addFailure() {
        failureCounter.incrementAndGet();
    }

    public void addError() {
        errorCounter.incrementAndGet();
    }

    public int getSuccess() {
        return successCounter.get();
    }

    public int getFailure() {
        return failureCounter.get();
    }

    public int getError() {
        return errorCounter.get();
    }

    public double reset() {
        double sum = accumulator.get();
        int n = successCounter.get();
        double mean = sum / (n - lastCheckPoint.get());
        lastCheckPoint.set(n);
        accumulator.set(0);
        return mean;
    }

    public void log() {
        int truncatedMean = (int) Math.floor(reset());
        LOGGER.log(Level.INFO, truncatedMean + "\t" + getSuccess() + "\t" + getFailure() + "\t" + getError());
    }

    public void run() throws InterruptedException {
        if (samplingInterval > 0) {
            while (isRunning) {
                if (this.monitored.size() == 0) {
                    break;
                }
                log();
                Thread.sleep(samplingInterval);
            }
            log();
        }
    }

    public void stop() {
        isRunning = false;
    }
}

package load;

import radius.RadiusAuthenticator;
import util.Sampler;

import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RadiusLoadGenerator {

    public static void main(String[] args) throws SocketException, InterruptedException {
        Options options = new Options(args);
        int samplingInterval = options.get(Options.SAMPLE, -1);
        int rows = options.get(Options.ROWS, -1);
        int iterations = options.get(Options.ITERATIONS, -1);
        int interval = options.get(Options.INTERVAL, 1000);
        String radius = options.get(Options.RADIUS_HOST);
        int radiusTimeout = options.get(Options.RADIUS_TIMEOUT, 10000);
        int radiusRetry = options.get(Options.RADIUS_RETRY, 1);
        long radiusDelay = options.get(Options.RADIUS_DELAY, 0);
        String testType = options.get(Options.TEST_TYPE, "accept");

        String radiusHost = radius.split(":")[0];
        int radiusPort = Integer.parseInt(radius.split(":")[1]);
        String radiusSecret = "secret";

        System.out.println("# Test type: " + testType);
        System.out.println("# Max rows: " + rows);
        System.out.println("# Iterations: " + iterations);
        System.out.println("# Interval between requests: " + interval + " ms");
        System.out.println("# Sampling interval: " + samplingInterval + " ms");
        System.out.println("# Radius host: " + radiusHost);
        System.out.println("# Radius port: " + radiusPort);
        System.out.println("# Radius delay: " + radiusDelay);

        options.validate();

        Sampler sampler = new Sampler(samplingInterval);
        for (int i = 0; i < rows; i++) {
            String id = UUID.randomUUID().toString();
            RadiusAuthenticator radiusAuthenticator = new RadiusAuthenticator(id, radiusHost, radiusPort, radiusTimeout, radiusRetry, radiusSecret, sampler);
            new Thread(radiusAuthenticator::run).start();
            new Thread(new Runner(radiusAuthenticator, iterations, interval)::run).start();
        }
        sampler.run();
    }

    public static class Runner {
        private final RadiusAuthenticator radiusAuthenticator;
        private final int iterations;
        private final long minDuration;

        public Runner(RadiusAuthenticator radiusAuthenticator, int iterations, long minDuration) {
            this.radiusAuthenticator = radiusAuthenticator;
            this.iterations = iterations;
            this.minDuration = minDuration;
        }

        public void run() {
            try {
                for (int j = 0; j < iterations; j++) {
                    long startTime = System.currentTimeMillis();
                    this.radiusAuthenticator.send("PHXDemo", "Connected196", System.currentTimeMillis());
                    long duration = System.currentTimeMillis() - startTime;
                    long sleep = Math.max(0, minDuration - duration);
                    if (sleep > 0) {
                        Thread.sleep(sleep);
                    }
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            } finally {
                radiusAuthenticator.stop();
            }
        }
    }

    static class Options {
        public static final String SAMPLE = "--sample";
        public static final String RADIUS_HOST = "--radius";
        public static final String ROWS = "--rows";
        public static final String ITERATIONS = "--iterations";
        public static final String RADIUS_TIMEOUT = "--radius-timeout";
        public static final String RADIUS_RETRY = "--radius-retry";
        public static final String RADIUS_DELAY = "--radius-delay";
        public static final String INTERVAL = "--interval";
        public static final String TEST_TYPE = "--type";

        private final Map<String, Object> options = new HashMap<>();

        Options(String[] args) {
            for (int i = 0; i < args.length; i++) {
                String key = args[i];
                Object value;
                switch (key) {
                    case RADIUS_HOST:
                    case TEST_TYPE:
                        value = args[++i];
                        break;
                    case SAMPLE:
                        value = Integer.parseInt(args[++i]);
                        break;
                    case ROWS:
                    case INTERVAL:
                    case ITERATIONS:
                    case RADIUS_TIMEOUT:
                    case RADIUS_RETRY:
                        value = Integer.valueOf(args[++i]);
                        break;
                    case RADIUS_DELAY:
                        value = Long.valueOf(args[++i]);
                        break;
                    default:
                        value = key;
                }
                options.put(key, value);
            }
        }

        public <T> T get(String key, T def) {
            @SuppressWarnings("unchecked") T t = (T) options.remove(key);
            if (t == null) {
                return def;
            }
            return t;
        }

        public <T> T get(String key) {
            @SuppressWarnings("unchecked") T t = (T) options.remove(key);
            if (t == null) {
                throw new RuntimeException("option not found: " + key);
            }
            return t;
        }

        public void validate() {
            if (!options.isEmpty()) {
                String result = String.join("\n", options.keySet());
                throw new RuntimeException("Unknown options: \n" + result);
            }
        }
    }

}

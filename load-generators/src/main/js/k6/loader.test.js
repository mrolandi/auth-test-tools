import http from "k6/http";
import encoding from "k6/encoding";
import crypto from "k6/crypto";
import RSAKey from "./jsrsasign/src/keyutil-1.0.js";


const lines = open("/Users/mans/onetouch-data.txt").split('\n')

export default function() {
    const line = lines[0];
    const fields = line.split('|');
    const profile = JSON.parse(encoding.b64decode(fields[5]));
    const auth = encoding.b64decode(fields[6]);
    const sign = JSON.parse(encoding.b64decode(fields[7]));
    const challengeResponse = { id: profile.id, algorithm: 'SHA1WithRSA' };
    const b64 = encoding.b64encode(JSON.stringify(challengeResponse));
    const params = { headers: { 'X-PhxID-Challenge-Response': b64 }};
    const url = "http://localhost:8444/pki/tokens/" + profile.id;
    const response = http.get(url, params);
    const challenge = response.headers['X-Phxid-Challenge'];
    const keyData = encoding.b64decode(sign['private_key']);
    const hash = crypto.sha1("hello world!", "base64");
    //var pk = forge.setPrivateKey(sign['private_key']);
    //console.log(Object.keys(pk.n));
    //var k = asdf.sha1.create();
    //k.update('hello world!', 'utf8');
    //pk.sign(hash);
    //console.log(Object.keys(forge));
};

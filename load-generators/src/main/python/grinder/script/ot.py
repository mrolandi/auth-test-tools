import base64

import onetouch.OneTouch
import radius.RadAuth
from HTTPClient import NVPair
from net.grinder.plugin.http import HTTPRequest
from net.grinder.script.Grinder import grinder

RADIUS_HOST = grinder.getProperties().get("grinder.radius_host")

f = open("/Users/mans/onetouch-data.txt")
http_request = HTTPRequest()

line = f.readline()
fields = line.split('|')

one_touch = onetouch.OneTouch()

radius_client = radius.DiscardingRadiusClient('127.0.0.1', 'secret')
SECRET_TOKEN = "a051ec7f14672f14ebeb"
TOKEN_LENGTH = 6


def get(uri, headers):
    nv_pairs = []
    for header_name, header_value in headers.iteritems():
        b64encoded_header = one_touch.writeValueAsString(header_value, True)
        nv_pairs.append(NVPair(header_name, b64encoded_header))
    return http_request.GET(uri, None, nv_pairs)


def get_challenge(uri, profile_id):
    header = {
        'id': profile_id,
        'algorithm': 'SHA1WithRSA'
    }
    http_response = get(uri, {'X-PhxID-Challenge-Response': header})
    return http_response.getHeader('X-Phxid-Challenge')


def get_assignments(uri, profile_id, signed_challenge):
    header = {
        'id': profile_id,
        'algorithm': 'SHA1WithRSA',
        'signature': signed_challenge
    }
    http_response = get(uri, {'X-PhxID-Challenge-Response': header})
    return one_touch.toMap(http_response.getText()).get('assignments')


def get_body(http_response):
    return one_touch.toMap(http_response.getText())


class TestRunner:
    def __init__(self):
        pass

    def __call__(self):
        radius_client.authenticate(fields[0], 'password')
        profile_data = base64.b64decode(fields[5])
        profile_data = one_touch.toMap(profile_data)
        auth_data = base64.b64decode(fields[6])
        auth_data = one_touch.toMap(auth_data)
        sign_data = base64.b64decode(fields[7])
        sign_data = one_touch.toMap(sign_data)
        auth_key = str(auth_data.get('private_key'))
        sign_key = str(sign_data.get('private_key'))
        profile_id = str(profile_data.get('id'))
        uri = "http://localhost:8444/pki/tokens/" + profile_id

        challenge = get_challenge(uri, profile_id)
        signed_challenge = str(one_touch.sign('SHA1WithRSA', auth_key, challenge))
        assignments = get_assignments(uri, profile_id, signed_challenge)

        print assignments.get(0)

        data = 'pid=' + profile_id + '&aid=form&ts='
        form_signature = {
            'id': 'form',
            'algorithm': 'SHA1WithRSA',
            'data': data,
            'content_type': 'text/plain',
            'content_encoding': 'none'
        }
        signature = str(one_touch.sign('SHA1WithRSA', sign_key, data))
        assignment_confirm = {
            'signature': form_signature,
            'signatures': [form_signature]
        }
